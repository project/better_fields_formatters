/*
*
*global Drupal:true
*
**/

(function ($, Drupal, drupalSettings) {

  'use strict';

  $('.js-description_readmore').moreLines({
    linecount: drupalSettings.expandingtextformatter.attributes.line_number,
    baseclass: 'b-description',
    basejsclass: 'js-description',
    classspecific: '_readmore',
    buttontxtmore: drupalSettings.expandingtextformatter.attributes.label_readmore_link,
    buttontxtless: drupalSettings.expandingtextformatter.attributes.label_readless_link,
    animationspeed: 250
  });
})(jQuery, Drupal, drupalSettings);

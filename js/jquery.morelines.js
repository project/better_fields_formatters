(function ($) {

  'use strict';

  /* morelines - simple to use, if you need to expand the text by various line - this is it
   *
   * <script>
   *   $(function() {
   *     $('.js-description_readmore').moreLines({
   *       linecount: 2,
   *       baseclass: 'b-description',
   *       basejsclass: 'js-description',
   *       classspecific: 'readmore',
   *       buttontxtmore: "read more",
   *       buttontxtless: "read less",
   *       animationspeed: 250
   *     });
   *   });
   * </script>
   *
   * V.Tsurule
   */
  $.fn.moreLines = function (options) {

    this.each(function () {

      var element = $(this);
      var baseclass = 'b-morelines_';
      var basejsclass = 'js-morelines_';
      var currentclass = 'section';
      var singleline = parseFloat(element.css('line-height'));
      var auto = 1;
      var fullheight = element.innerHeight();
      var settings = $.extend({
        linecount: auto,
        baseclass: baseclass,
        basejsclass: basejsclass,
        classspecific: currentclass,
        buttontxtmore: 'more lines',
        buttontxtless: 'less lines',
        animationspeed: auto
      }, options);
      var ellipsisclass = settings.baseclass + settings.classspecific + '_ellipsis';
      var buttonclass = settings.baseclass + settings.classspecific + '_button';
      var wrapcss = settings.baseclass + settings.classspecific + '_wrapper';
      var wrapjs = settings.basejsclass + settings.classspecific + '_wrapper';
      var wrapper = $('<div>').addClass(wrapcss + ' ' + wrapjs).css({'max-width': element.css('width')});
      var linescount = singleline * settings.linecount;

      element.wrap(wrapper);

      if (element.parent().not(wrapjs)) {

        if (fullheight > linescount) {

          element.addClass(ellipsisclass).css({
            'min-height': linescount,
            'max-height': linescount,
            'overflow': 'hidden'
          });

          var moreLinesButton = $('<div>', {
            class: buttonclass,
            click: function () {

              element.toggleClass(ellipsisclass);
              $(this).toggleClass(buttonclass + '_active');

              if (element.css('max-height') !== 'none') {
                element.css({
                  'height': linescount,
                  'max-height': ''
                }).animate({height: fullheight}, settings.animationspeed, function () {
                  moreLinesButton.html(settings.buttontxtless);
                });

              }
              else {
                element.animate({height: linescount}, settings.animationspeed, function () {
                  moreLinesButton.html(settings.buttontxtmore);
                  element.css('max-height', linescount);
                });
              }
            },

            html: settings.buttontxtmore
          });

          element.after(moreLinesButton);

        }
      }
    });

    return this;
  };

}(jQuery));

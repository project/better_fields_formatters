<?php

namespace Drupal\better_fields_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextTrimmedFormatter;

/**
 * Plugin implementation of the 'expanding_text' formatter.
 *
 * @FieldFormatter(
 *   id = "expanding_text",
 *   label = @Translation("Expanding Text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   quickedit = {
 *     "editor" = "form"
 *   }
 * )
 */
class ExpandingTextFormatter extends TextTrimmedFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'line_number' => '2',
      'label_readmore_link' => t('Read more'),
      'label_readless_link' => t('Read less'),
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['line_number'] = [
      '#title' => $this->t('Number line'),
      '#type' => 'number',
      '#field_suffix' => $this->t('line(s)'),
      '#default_value' => $this->getSetting('line_number'),
      '#description' => $this->t('Number of lines to display.'),
      '#min' => 1,
      '#required' => TRUE,
    ];
    $element['label_readmore_link'] = [
      '#title' => $this->t('Expanded label'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('label_readmore_link'),
      '#description' => $this->t('The label of read more link.'),
      '#required' => TRUE,
    ];
    $element['label_readless_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collapsed label'),
      '#description' => $this->t('The label of read less link.'),
      '#default_value' => $this->getSetting('label_readless_link'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Line number: @line_number line(s)', ['@line_number' => $this->getSetting('line_number')]);
    $summary[] = $this->t('Label read more link: @label_readmore_link', ['@label_readmore_link' => $this->getSetting('label_readmore_link')]);
    $summary[] = $this->t('Label read less link: @label_readless_link', ['@label_readless_link' => $this->getSetting('label_readless_link')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $attributes = [];

    if (!empty($this->getSetting('line_number'))) {
      $attributes['line_number'] = $this->getSetting('line_number');
    }

    if (!empty($this->getSetting('label_readless_link'))) {
      $attributes['label_readless_link'] = $this->getSetting('label_readless_link');
    }

    if (!empty($this->getSetting('label_readmore_link'))) {
      $attributes['label_readmore_link'] = $this->getSetting('label_readmore_link');
    }

    foreach ($items as $delta => $item) {
      $original = $item;
      $elements[$delta] = [
        '#theme' => 'expandingtextformatter',
        '#attributes' => $attributes,
        '#contentdata' => $original,
        '#attached' => [
          'library' => ['better_fields_formatters/expanded_text_formatter'],
          'drupalSettings' => [
            'expandingtextformatter' => [
              'attributes' => $attributes,
            ],
          ],
        ],
      ];
    }

    return $elements;
  }

}

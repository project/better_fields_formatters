INTRODUCTION
------------
 
This module provides a new fields formatter for many type of fields. 
This module allows you to change how the content is being display (formatted) 
by providing new format types.


INSTALLATION
------------

Install as usual, see
https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 
for further information.


MAINTAINERS
-----------

Current maintainers:

 * Akram AMOURI (https://www.drupal.org/u/aamouri)
 * Asmaa KHALFI (https://www.drupal.org/u/asmaakhalfi)
 * Khalil CHARFI (https://www.drupal.org/u/ckhalilo)
 
REQUIREMENTS
------------

Last version of Drupal 8.x.


CONFIGURATION
------------- 

No specific configuration for the first version.
